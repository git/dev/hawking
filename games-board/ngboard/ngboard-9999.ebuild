# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils games subversion

DESCRIPTION="Next generation chess interface"
HOMEPAGE="http://ngboard.sourceforge.net/"

ESVN_REPO_URI="http://ngboard.svn.sourceforge.net/svnroot/ngboard/trunk"
ESVN_BOOTSTRAP="./autogen.sh"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="dev-cpp/cairomm
	dev-cpp/gtkmm
	dev-libs/libpcre
	dev-cpp/libxmlpp
	gnome-base/gnome-common
	gnome-base/librsvg
	media-libs/gstreamer"
RDEPEND="${DEPEND}"

pkg_setup() {
	if ! built_with_use dev-libs/libpcre cxx; then
		eerror "dev-libs/libpcre needs to be built with cxx USE flag"
		die "dev-libs/libpcre built without cxx"
	fi
}

src_unpack() {
	subversion_src_unpack
	cd "${S}"

	epatch "${FILESDIR}"/void-ptr-64bit.patch
}

src_compile() {
	egamesconf
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"

	insinto /usr/share/games/${PN}/sounds
	doins -r sounds || die "doins failed"

	prepgamesdirs
}
